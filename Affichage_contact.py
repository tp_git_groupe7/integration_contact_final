class GestionnaireContacts:
    
    def afficher_contacts(self):
        if not self.contacts:
            print("Aucun contact trouvé.")
        else:
            print("Liste des contacts :")
            for i, contact in enumerate(self.contacts, start=1):
                print(f"{i}. Nom : {contact.nom}, Numéro : {contact.numero}")

